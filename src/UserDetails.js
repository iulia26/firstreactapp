import React from "react";

const divStyle = {
  opacity: "1",
  height: "700px",
};
const DetailsView = ({ user }) => (
  <>
    <div style={divStyle}>
      <p>Gender : {user.gender}</p>
      <p>Cell : {user.cell}</p>
      <p> Email : {user.email}</p>
      <p>Username : {user.login.username}</p>
      <p>Password : {user.login.password}</p>
      <p>City : {user.location.city} </p>
    </div>
  </>
);
export { DetailsView };



class MouseTracker extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.state = { x: 0, y: 0 };
  }

  handleMouseMove(event) {
    this.setState({
      x: event.clientX,
      y: event.clientY,
    });
  }

  render() {
    return (
      <div style={{ height: "100vh" }} onMouseMove={this.handleMouseMove}>
        <h1>Move the mouse around!</h1>
        <p>
          The current mouse position is ({this.state.x}, {this.state.y})
        </p>
      </div>
    );
  }
}
export { MouseTracker };
