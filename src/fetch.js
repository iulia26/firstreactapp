import React from "react";
import ReactDOM from "react-dom/client";
import { BtnsDeleteInfoContainer } from "./components/InfoButton";
import { MouseTracker } from "./UserDetails";

const TEMPLATE_ID = "user-row"
const LIST_ID = "list"
const BASE_URL = "https://randomuser.me/api"
const RESULT_COUNT = 200
const SEED = "ontegrainternship"
const USER_DETAILS = "userDetails_"
const QUERY_PARAMS = new URLSearchParams({ results: RESULT_COUNT, seed: SEED })
const DROPDOWN_ID = "infoDrop_"



/**
 * Builds a `li` element which will be injected in the `ul`
 * @param {Object} user User to render
 */

function renderUser(user) {
	let userEmail = user.email
	const template = document.getElementById(TEMPLATE_ID)
	const templateClone = template.content.cloneNode(true)
	
	const templateContainer = templateClone.querySelector("li")
	const btnsContainer = templateClone.querySelector('#buttonContainer')
	const root = ReactDOM.createRoot(btnsContainer);
		root.render(<BtnsDeleteInfoContainer myuser={user}/>);
	//root.render(<MouseTracker />);
	
	
	templateContainer.id = userEmail
	// Render image
	const image = templateContainer.querySelector("img")
	image.src = user.picture.medium
	
	//return a NodeList
	let detailsDiv =  templateContainer.querySelector("#user-details")
	detailsDiv.id = USER_DETAILS.concat(user.email)
	let divFirstName = document.createElement("div")
	let node = document.createTextNode(`First name:${user.name.first}`)
	divFirstName.appendChild(node)
	detailsDiv.appendChild(divFirstName)

	let divLastName = document.createElement("div")
	node = document.createTextNode(`Last name:${user.name.last}`)
	divLastName.appendChild(node)
	detailsDiv.appendChild(divLastName)

	let divEmail = document.createElement("div")
	node = document.createTextNode(`Email : ${user.email}`)
	divEmail.appendChild(node)
	detailsDiv.appendChild(divEmail)

	let divAge = document.createElement("div")
	node = document.createTextNode(`Age : ${user.dob.age}`)
	divAge.appendChild(node)
	detailsDiv.appendChild(divAge)

	//get the dropdown
	let drop = templateContainer.querySelector("#dropdownInfo")
	drop.id = DROPDOWN_ID.concat(user.email)
	return templateClone
}

/**
 * Renders the given list of users in the page
 * @param {Array} users List of users to render
 */
function injectUsersInPage(users) {
	const list = document.getElementById(LIST_ID)
	if (list) {
		
		users.forEach(user => {
			const userElement = renderUser(user)
			list.appendChild(userElement)
		
		})
	}
}

function fetchUsers() {
	fetch(`${BASE_URL}/?${QUERY_PARAMS.toString()}`, { method: "GET" })
		.then(response => response.json())
		.then(response => {
			// TODO Handle response
			console.log(response.results)
			injectUsersInPage(response.results)
		})
		.catch(err => {
			// TODO Handle error
			console.error(err)
		})
		.finally(() => {
			// TODO Handle finally - maybe loading state
			
		})
}



export default fetchUsers;
