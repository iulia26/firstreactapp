import React, { Component } from "react";
import { Subject } from "rxjs";
import { HeaderRoot } from "..";
import "../styles/Header.css";
// import {ReactComponent as Logo} from './pum.svg';
const BTN_DELETE = "btnDelete_";
const BTN_INFO = "btnInfo_";
const DROPDOWN_ID = "infoDrop_";
var actionsCnt = 0;
var objActionsCnt = { cnt: 0 }; //daca folosesc un obiect atunci cand ii schimb valoarea se va modifica si in MyHeader//adica in js obiectele sunt prin referinta trasnmise?
var btnClicked = false;
const headerSubject = new Subject();
headerSubject.subscribe({
  next: () => {
    actionsCnt += 1;
    HeaderRoot.render(<MyHeader />);
  }
});

function details(user) {
  let finalString = "{ \n";
  let gender = user.gender;
  let email = user.email;
  let cell = user.cell;
  let username = user.login.username;
  let password = user.login.password;
  let city = user.location.city;
  finalString += "Gender : ";
  finalString += gender;
  finalString += "\n";
  finalString += "Email : ";
  finalString += email;
  finalString += "\n";
  finalString += "Cell : ";
  finalString += cell;
  finalString += "\n";
  finalString += "Username : ";
  finalString += username;
  finalString += "\n";
  finalString += "Password : ";
  finalString += password;
  finalString += "\n";
  finalString += "City : ";
  finalString += city;
  finalString += "\n";

  finalString += "}";
  return finalString;
}
class MyHeader extends React.Component {
  render() {
    return (
      <nav>
        <div className="Header">
          <p>Action count is set to {actionsCnt} </p>
        </div>
      </nav>
    );
  }
}

const BtnsDeleteInfoContainer = ({ myuser }) => (
  <>
    <div>
      <button
        id={BTN_INFO.concat(myuser.email)}
        className="button-1"
        onClick={() => {
          const infoDiv = document.getElementById(
            DROPDOWN_ID.concat(myuser.email)
          );
          var str = details(myuser); //aici puteam sa pun o componenta noua
          if (!btnClicked) {
            btnClicked = true;
            infoDiv.style.opacity = "1";
            infoDiv.style.height = "700px";
            infoDiv.textContent = str;
          } else {
            infoDiv.style.opacity = "0";
            infoDiv.style.height = "0px";
            btnClicked = false;
          }
          headerSubject.next();
        }}
      >
        Info
      </button>
    </div>
    <div>
      <button
        id={BTN_DELETE.concat(myuser.email)}
        className="button-1"
        onClick={() => {
          const userDiv = document.getElementById(myuser.email);
          userDiv.remove();
          headerSubject.next();
        }}
        style={{ backgroundColor: "#EC5632" }}
      >
        Delete
      </button>
    </div>
  </>
);
export { BtnsDeleteInfoContainer };
export { MyHeader };
