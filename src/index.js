import ReactDOM from 'react-dom/client';
import './index.css';
import './styles/myStyle.css';
import 'bootstrap/dist/css/bootstrap.css'
import fetchUsers from './fetch';
import { MyHeader } from "./components/InfoButton";


const HeaderRoot = ReactDOM.createRoot(document.querySelector('#firstHeader'));
HeaderRoot.render(<MyHeader />);
window.addEventListener("load", function () {
	fetchUsers()
})

export {HeaderRoot};
